require 'rails_helper'

RSpec.describe 'User Followers', type: :request do
  describe 'POST /api/v1/users/follow' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:following_user) { FactoryBot.create(:user) }

    context 'with valid parameters' do
      before do
        post '/api/v1/users/follow', params: {
          user_follower: {
            user_id: following_user.id
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns the user_id' do
        expect(json['user_id']).to eq(following_user.id)
      end

      it 'returns the follower_id' do
        expect(json['follower_id']).to eq(user.id)
      end
    end

    context 'with the same user ID' do
      before do
        post '/api/v1/users/follow', params: {
          user_follower: {
            user_id: user.id
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with blank user ID' do
      before do
        post '/api/v1/users/follow', params: {
          user_follower: {
            user_id: ""
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end