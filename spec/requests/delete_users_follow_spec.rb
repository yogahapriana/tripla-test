require 'rails_helper'

RSpec.describe 'User Followers', type: :request do
  describe 'DELETE /api/v1/users/follow' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:following_user) { FactoryBot.create(:user) }

    context 'with valid parameters' do
      let!(:user_follower) { FactoryBot.create(:user_follower, user_id: following_user.id, follower_id: user.id) }

      before do
        delete '/api/v1/users/follow', params: {
          user_follower: {
            user_id: following_user.id
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns data' do
        expect(json['data']).to eq(true)
      end

      it 'unfollow user' do
        expect(UserFollower.where(user_id: following_user.id, follower_id: user.id).count).to eq(0)
      end
    end

    context 'with same user ID' do
      let!(:user_follower) { FactoryBot.create(:user_follower, user_id: following_user.id, follower_id: user.id) }

      before do
        delete '/api/v1/users/follow', params: {
          user_follower: {
            user_id: user.id
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with blank user ID' do
      let!(:user_follower) { FactoryBot.create(:user_follower, user_id: following_user.id, follower_id: user.id) }

      before do
        delete '/api/v1/users/follow', params: {
          user_follower: {
            user_id: ""
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end