require 'rails_helper'

RSpec.describe 'User Sleep Records', type: :request do
  describe 'PUT /api/v1/users/sleep_records/clock_in' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:user2) { FactoryBot.create(:user) }
    let!(:existing_user_sleep_record) {
      FactoryBot.create(:user_sleep_record,
                        clocked_out_at: nil,
                        sleep_duration: nil,
                        sleep_duration_label: nil,
                        is_active: true,
                        user_id: user.id,
                        user_name: user.name) }

    context 'with valid parameters' do
      before do
        put '/api/v1/users/sleep_records/clock_out', params: {
          user_sleep_record: {
            clocked_out_at: Time.now.strftime("%F %T" )
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns the clocked_in_at' do
        expect(json['clocked_in_at']).to eq(existing_user_sleep_record.clocked_in_at.as_json)
      end

      it 'returns the clocked_out_at' do
        expect(json['clocked_out_at']).to be_present
      end

      it 'returns the sleep_duration' do
        expect(json['sleep_duration']).to be_present
      end

      it 'returns the sleep_duration_label' do
        expect(json['sleep_duration_label']).to be_present
      end

      it 'returns the is_active' do
        expect(json['is_active']).to eq(false)
      end

      it 'returns the user_id' do
        expect(json['user_id']).to eq(existing_user_sleep_record.user_id)
      end

      it 'returns the user_name' do
        expect(json['user_name']).to eq(existing_user_sleep_record.user_name)
      end
    end

    context 'with invalid user ID' do
      before do
        put '/api/v1/users/sleep_records/clock_out', params: {
          user_sleep_record: {
            clocked_out_at: Time.now.strftime("%F %T" )
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user2.id
        }
      end

      it 'returns an unprocessable entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with blank clocked_out_at' do
      before do
        put '/api/v1/users/sleep_records/clock_out', params: {
          user_sleep_record: {
            clocked_out_at: ""
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end