require 'rails_helper'

RSpec.describe 'User Sleep Records', type: :request do
  describe 'GET /api/v1/users/sleep_records' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:user2) { FactoryBot.create(:user) }

    context 'with user that already have sleep record' do
      before do
        FactoryBot.create_list(:user_sleep_record, 10, user_id: user.id)
        @first_sleep_record_id = UserSleepRecord.where(user_id: user.id).order("created_at DESC")[0].id

        get '/api/v1/users/sleep_records', headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns all user sleep records' do
        expect(json.size).to eq(10)
      end

      it 'returns correct order' do
        expect(json[0]["id"]).to eq(@first_sleep_record_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(:success)
      end
    end

    context 'with user that dont have sleep record' do
      before do
        FactoryBot.create_list(:user_sleep_record, 10, user_id: user.id)

        get '/api/v1/users/sleep_records', headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user2.id
        }
      end

      it 'returns all user sleep records' do
        expect(json.size).to eq(0)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(:success)
      end
    end
  end
end