require 'rails_helper'

RSpec.describe 'User Sleep Records', type: :request do
  describe 'GET /api/v1/users/sleep_records/my_following/previous_week' do
    let!(:user) { FactoryBot.create(:user) }
    let!(:user_sleep_record_last_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.last_week.beginning_of_week,
                        clocked_in_at: Date.today.last_week.beginning_of_week,
                        clocked_out_at: Date.today.last_week.beginning_of_week + 3.hours,
                        sleep_duration: 3.hours.to_i,
                        user_id: user.id)
    }
    let!(:user_sleep_record_this_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.beginning_of_week,
                        clocked_in_at: Date.today.beginning_of_week,
                        clocked_out_at: Date.today.beginning_of_week + 4.hours,
                        sleep_duration: 4.hours.to_i,
                        user_id: user.id)
    }

    let!(:user2) { FactoryBot.create(:user) }
    let!(:user2_sleep_record_last_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.last_week.beginning_of_week,
                        clocked_in_at: Date.today.last_week.beginning_of_week,
                        clocked_out_at: Date.today.last_week.beginning_of_week + 4.hours,
                        sleep_duration: 4.hours.to_i,
                        user_id: user2.id)
    }
    let!(:user2_sleep_record_this_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.beginning_of_week,
                        clocked_in_at: Date.today.beginning_of_week,
                        clocked_out_at: Date.today.beginning_of_week + 5.hours,
                        sleep_duration: 5.hours.to_i,
                        user_id: user2.id)
    }

    let!(:user3) { FactoryBot.create(:user) }
    let!(:user3_sleep_record_last_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.last_week.beginning_of_week,
                        clocked_in_at: Date.today.last_week.beginning_of_week,
                        clocked_out_at: Date.today.last_week.beginning_of_week + 5.hours,
                        sleep_duration: 5.hours.to_i,
                        user_id: user3.id)
    }
    let!(:user3_sleep_record_this_week) {
      FactoryBot.create(:user_sleep_record,
                        created_at: Date.today.beginning_of_week,
                        clocked_in_at: Date.today.beginning_of_week,
                        clocked_out_at: Date.today.beginning_of_week + 6.hours,
                        sleep_duration: 6.hours.to_i,
                        user_id: user3.id)
    }

    let!(:user_follower) { FactoryBot.create(:user_follower, user_id: user2.id, follower_id: user.id) }
    let!(:user_follower2) { FactoryBot.create(:user_follower, user_id: user3.id, follower_id: user.id) }

    context "with valid parameter" do
      before do
        get '/api/v1/users/sleep_records/my_following/previous_week', headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns all user sleep records' do
        expect(json.size).to eq(2)
      end

      it 'returns correct order' do
        expect(json[0]["id"]).to eq(user3_sleep_record_last_week.id)
      end
    end

    context "with empty data" do
      before do
        get '/api/v1/users/sleep_records/my_following/previous_week', headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user2.id
        }
      end

      it 'returns all user sleep records' do
        expect(json.size).to eq(0)
      end
    end
  end
end