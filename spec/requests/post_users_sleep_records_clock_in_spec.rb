require 'rails_helper'

RSpec.describe 'User Sleep Records', type: :request do
  describe 'POST /api/v1/users/sleep_records/clock_in' do
    let!(:user) { FactoryBot.create(:user) }
    let(:valid_user_sleep_record) {
      UserSleepRecord.new(
        clocked_in_at: 1.day.ago.strftime("%F %T"),
        is_active: true,
        user_id: user.id,
        user_name: user.name
      )
    }

    context 'with valid parameters' do
      before do
        post '/api/v1/users/sleep_records/clock_in', params: {
          user_sleep_record: {
            clocked_in_at: valid_user_sleep_record.clocked_in_at
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns the clocked_in_at' do
        expect(json['clocked_in_at']).to eq(valid_user_sleep_record.clocked_in_at.as_json)
      end

      it 'returns the clocked_out_at' do
        expect(json['clocked_out_at']).to eq(nil)
      end

      it 'returns the sleep_duration' do
        expect(json['sleep_duration']).to eq(nil)
      end

      it 'returns the sleep_duration_label' do
        expect(json['sleep_duration_label']).to eq(nil)
      end

      it 'returns the is_active' do
        expect(json['is_active']).to eq(true)
      end

      it 'returns the user_id' do
        expect(json['user_id']).to eq(valid_user_sleep_record.user_id)
      end

      it 'returns the user_name' do
        expect(json['user_name']).to eq(valid_user_sleep_record.user_name)
      end
    end

    context 'with nil user ID' do
      before do
        post '/api/v1/users/sleep_records/clock_in', params: {
          user_sleep_record: {
            clocked_in_at: valid_user_sleep_record.clocked_in_at
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => nil
        }
      end

      it 'returns an unauthorized status' do
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'with invalid user ID' do
      before do
        post '/api/v1/users/sleep_records/clock_in', params: {
          user_sleep_record: {
            clocked_in_at: valid_user_sleep_record.clocked_in_at
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => -1
        }
      end

      it 'returns an unauthorized status' do
        expect(response).to have_http_status(:unauthorized)
      end
    end

    context 'with invalid params' do
      before do
        post '/api/v1/users/sleep_records/clock_in', params: {
          user_sleep_record: {
            clocked_in_at: nil
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with invalid clocked_in_at' do
      before do
        post '/api/v1/users/sleep_records/clock_in', params: {
          user_sleep_record: {
            clocked_in_at: "clocked_in_at"
          }
        }, headers: {
          'HTTP_ACCEPT' => 'application/json',
          'X-User-ID' => user.id
        }
      end

      it 'returns an unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end
end