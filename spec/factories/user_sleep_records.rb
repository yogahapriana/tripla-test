FactoryBot.define do
  factory :user_sleep_record do
    clocked_in_at { 1.day.ago.strftime("%F %T" ) }
    clocked_out_at { Time.now.strftime("%F %T" ) }
    sleep_duration { 86400 }
    sleep_duration_label { ActionController::Base.helpers.distance_of_time_in_words(clocked_in_at, clocked_out_at)   }
    is_active { false }
    user_id { 1 }
    user_name { Faker::Name.name }
  end
end
