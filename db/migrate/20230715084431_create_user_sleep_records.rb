class CreateUserSleepRecords < ActiveRecord::Migration[7.0]
  def change
    create_table :user_sleep_records do |t|
      t.timestamp :clocked_in_at
      t.timestamp :clocked_out_at
      t.integer :sleep_duration
      t.string :sleep_duration_label
      t.boolean :is_active
      t.integer :user_id
      t.string :user_name

      t.timestamps
    end
  end
end
