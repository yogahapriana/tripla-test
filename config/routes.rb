Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      namespace :users do
        post "/sleep_records/clock_in", to: "user_sleep_records#clock_in"
        put "/sleep_records/clock_out", to: "user_sleep_records#clock_out"
        get "/sleep_records", to: "user_sleep_records#index"
        get "/sleep_records/my_following/previous_week", to: "user_sleep_records#my_following_previous_week"
      end

      post "/users/follow", to: "users#follow"
      delete "/users/follow", to: "users#unfollow"
    end
  end
end
