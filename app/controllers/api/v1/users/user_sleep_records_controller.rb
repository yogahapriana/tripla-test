# frozen_string_literal: true

class Api::V1::Users::UserSleepRecordsController < ApplicationController
  before_action :authenticate_user!

  def clock_in
    @user_sleep_record = UserSleepRecord.create user_sleep_record_params.merge(
      user_id: current_user.id,
      user_name: current_user.name
    )

    if @user_sleep_record.valid?
      render json: @user_sleep_record, status: :created
    else
      render json: @user_sleep_record.errors, status: :unprocessable_entity
    end
  end

  def clock_out
    @user_sleep_record = UserSleepRecord.clock_out user_sleep_record_params.merge(user_id: current_user.id)

    if @user_sleep_record.valid?
      render json: @user_sleep_record, status: :ok
    else
      render json: @user_sleep_record.errors, status: :unprocessable_entity
    end
  end

  def index
    @user_sleep_records = UserSleepRecord.my_list current_user.id
    render json: @user_sleep_records, status: :ok
  end

  def my_following_previous_week
    @user_sleep_records = UserSleepRecord.my_following_previous_week current_user.id
    render json: @user_sleep_records, status: :ok
  end

  private

  def user_sleep_record_params
    params.require(:user_sleep_record).permit(:clocked_in_at, :clocked_out_at)
  end
end