# frozen_string_literal: true

class Api::V1::UsersController < ApplicationController
  before_action :authenticate_user!

  def follow
    @user_follower = UserFollower.follow user_follower_params.merge(follower_id: current_user.id)

    if @user_follower.valid?
      render json: @user_follower, status: :created
    else
      render json: @user_follower.errors, status: :unprocessable_entity
    end
  end

  def unfollow
    @user_follower = UserFollower.unfollow user_follower_params.merge(follower_id: current_user.id)

    if @user_follower.valid?
      render json: { data: true }, status: :created
    else
      render json: @user_follower.errors, status: :unprocessable_entity
    end
  end

  private

  def user_follower_params
    params.require(:user_follower).permit(:user_id)
  end
end