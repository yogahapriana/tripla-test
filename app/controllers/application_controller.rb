class ApplicationController < ActionController::API
  protected

  def authenticate_user!
    if request.headers['X-User-ID'].blank?
      return render json: { errors: ["Unauthorized"] }, status: :unauthorized
    end

    user = User.find_by(id: request.headers['X-User-ID'])
    if user.blank?
      render json: { errors: ["Unauthorized"] }, status: :unauthorized
    end
  end

  def current_user
    user = User.find_by(id: request.headers['X-User-ID'])

    if user.present?
      user
    else
      nil
    end
  end
end
