class UserSleepRecord < ApplicationRecord
  after_initialize do
    if self.new_record?
      self.is_active = true
    end
  end

  validates :clocked_in_at, presence: true

  def self.clock_out(params)
    user_sleep_record = UserSleepRecord.where(is_active: true, user_id: params[:user_id]).first
    clocked_out_at = params[:clocked_out_at]
    new_user_sleep_record = UserSleepRecord.new

    if clocked_out_at.blank?
      new_user_sleep_record.errors.add(:clocked_out_at, "can't be blank")
      return new_user_sleep_record
    end

    if user_sleep_record.present?
      user_sleep_record.update(
        clocked_out_at: clocked_out_at,
        is_active: false,
        sleep_duration: clocked_out_at.to_time - user_sleep_record.clocked_in_at,
        sleep_duration_label: ActionController::Base.helpers.distance_of_time_in_words(user_sleep_record.clocked_in_at, clocked_out_at.to_time)
      )
    else
      new_user_sleep_record.errors.add(:base, "not found")
      user_sleep_record = new_user_sleep_record
    end

    user_sleep_record
  end

  def self.my_list(user_id)
    UserSleepRecord.where(user_id: user_id).order("created_at DESC")
  end

  def self.my_following_previous_week(user_id)
    user_ids = UserFollower.where(follower_id: user_id).pluck(:user_id)

    UserSleepRecord.where(
      user_id: user_ids,
      clocked_in_at: Date.today.last_week.beginning_of_week..Date.today.last_week.end_of_week
    ).order("sleep_duration DESC")
  end
end
