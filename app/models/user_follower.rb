class UserFollower < ApplicationRecord
  validates :follower_id, :user_id, presence: true
  validates :follower_id, uniqueness: { scope: :user_id }

  def self.follow(params)
    if params[:user_id].to_i.eql?(params[:follower_id].to_i)
      user_follower = UserFollower.new
      user_follower.errors.add(:follower_id, "not valid")
    else
      user_follower = UserFollower.create(params)
    end

    user_follower
  end

  def self.unfollow(params)
    if params[:user_id].to_i.eql?(params[:follower_id].to_i)
      user_follower = UserFollower.new
      user_follower.errors.add(:follower_id, "not valid")
    else
      user_follower = UserFollower.find_by(params)

      if user_follower.present?
        user_follower.destroy
      else
        user_follower = UserFollower.new
        user_follower.errors.add(:follower_id, "not valid")
      end
    end

    user_follower
  end
end
